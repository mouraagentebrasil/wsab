<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 26/10/2016
 * Time: 15:44
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class AytyLog extends Model
{
    protected $guarded = ['ayty_id'];
    protected $table = 'ayty_logs';
    public $timestamps = false;

    public static function saveAytyLog($token,$sentido,$macro,$fluxo,$linha,$obj_json,$lead_id,$ip)
    {
        $saveAytyLog = self::create([
            'ayty_process'=>$token,
            'ayty_date'=>date('Y-m-d H:i:s'),
            'ayty_sentido'=>$sentido,
            'ayty_macro'=>$macro,
            'ayty_fluxo'=>$fluxo,
            'ayty_linha'=>$linha,
            'ayty_json'=>$obj_json,
            'lead_id'=>$lead_id,
            'lead_ip'=>$ip,
        ]);
        return $saveAytyLog ? true: false;
    }

    public static function codProcess($length = 24)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function formataLinhaLeadAytyLog($data)
    {
        return $data['lead_id']."-".$data['lead_nome']."-".$data['lead_dddtelefone']."-".$data['lead_email'];
    }

    public static function formataLinhaCardAytyLog($data)
    {
        return $data['plan_id']."-".$data['plan_nome']."-".$data['plan_parcelas']."-".$data['plan_valor']."-".$data['plan_superlogica'];
    }

    public static function formataLinhaPlanoAytyLog($data){
        return $data['plan_nome']."-".$data['plan_logica']."-".$data['plan_parcelas']
                ."-".$data['plan_valor']."-".$data['plan_superlogica'];
    }
}