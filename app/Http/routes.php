<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return view('readme');
});

$app->post('/new_lead','LeadController@newLead');

$app->post('/update_lead','LeadController@updateLead');

$app->post('/update_card','LeadController@updateCard');

$app->get('/simula_ayty_inserts/{data_inicial?}/{data_final?}','LeadController@simulaAytyInserts');

$app->get('/simula_ayty_updates','LeadController@simulaAytyUpdates');

