<?php

namespace App\Http\Controllers;

use App\AytyLog;
use Laravel\Lumen\Routing\Controller as BaseController;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    //
    public $client = null;

    public function __construct()
    {
        $this->client = new Client();
    }
    //usado para testar os parametros que serão passados e valida-los
    public $validator = [
        'new_lead'=>['lead_id', 'lead_nome', 'lead_dddtelefone', 'lead_email', 'lead_ip','fluxo','lead_status'],
        'update_lead'=>['lead_id', 'fluxo', 'lead_status'],
        'update_card'=>['lead_id','card_id','fluxo','lead_status'],
        'plan_update'=>['plan_id','plan_nome','plan_logica','plan_superlogica','plan_descricao','plan_valor'],
        'new_plan'=>['plan_nome','plan_status','plan_logica','plan_descricao','plan_parcelas','plan_valor']
    ];

    public function validar($data,$url,$execeptions)
    {
        $arrErro = [];
        if(count($data) > count($this->validator[$url])){
            $arrErro[] = "numero de parametros excede o permitido";
            return $arrErro;
        }
        foreach ($this->validator[$url] as $item):
            if($execeptions != null && in_array($item,$execeptions)){
                continue;
            }
            if(!isset($data[$item])){
                $arrErro[] = "campo ".$item." não passado";
            } elseif($data[$item]==""){
                $arrErro[] = "campo ".$item." está nullo";
            }
        endforeach;
        return $arrErro;
    }

    public function getToken()
    {
        try {
            $credentials = json_encode(['username'=>getenv('WS_AYTY_LOGIN'),'password'=>getenv('WS_AYTY_PASS')]);
            $r = $this->client->request('POST',getenv('WS_AYTY_BASE')."Login",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$credentials
            ]);
            $token = json_decode($r->getBody()->getContents());
            //var_dump(json_encode($token)); exit;
            //var_dump($token->token); exit;
            //Gera Log
            AytyLog::saveAytyLog($token->token,'ab->ayty','linear','login','',json_encode($token),'','0.0.0.0');
            return $token->token;
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/login_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            AytyLog::saveAytyLog(AytyLog::codProcess(),'ab->ayty','linear','login','',$e->getMessage(),'','0.0.0.0');
            return null;
        }
    }
}