<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\AytyLog;
use App\Lead;
class LeadController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function newLead(Request $request)
    {
        $data = $request->all();
        $response = [];
        $valida = $this->validar($data,'new_lead',['lead_ip','lead_email','fluxo']);
        //var_dump($valida); exit;
        if(!empty($valida)){
            return response()->json(['erros'=>$valida],400);
        }
        //passa o token para completar a requisição
        $response['token']=$this->getToken();
        //var_dump($data); exit;
        try {
            $data['lead_status']='lead';
            $response['leads']=[$data];
            $body = json_encode($response);
            $r = $this->client->request('POST',getenv('WS_AYTY_BASE')."Lead/New",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$body
            ]);
            $response = $r->getBody()->getContents();
            //var_dump($response); exit;
            $respJson = json_decode($response);
            $msg = [];
            if(!isset($respJson->count_error)) {
                $msg['msg'] = "lead_inserido_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_gravar_lead";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLog::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','inserir lead',
                AytyLog::formataLinhaLeadAytyLog($data),$response,$data['lead_id'],$data['lead_ip']
            );
            file_put_contents(storage_path('logs/ayty_logs/new_lead_'.date('Y-m-d').'-'.time().".txt"),$response);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/new_lead_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            return response()->json(['message'=>'error','status'=>'error'],400);
        }
    }

    public function updateLead(Request $request)
    {
        $data = $request->all();
        $response = [];
        $valida = $this->validar($data,'update_lead',['lead_ip','lead_email']);
        if(!empty($valida)){
            return response()->json(['erros'=>$valida],400);
        }
        //passa o token para completar a requisição
        $response['token']=$this->getToken();
        try {

            $data['lead_status']='lead';
            $response['leads']=[$data];
            $body = json_encode($response);
            $r = $this->client->request('POST',getenv('WS_AYTY_BASE')."Lead/Update",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$body
            ]);
            $response = $r->getBody()->getContents();
            $respJson = json_decode($response);
            $msg = [];
            if(!isset($respJson->count_error)) {
                $msg['msg'] = "lead_atualizado_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_atualizar_lead";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLog::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','atualizar lead',
                '',$response,$data['lead_id'],''
            );
            file_put_contents(storage_path('logs/ayty_logs/update_lead_'.date('Y-m-d').'-'.time().".txt"),$response);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/update_lead_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            return response()->json(['message'=>'error','status'=>'error'],400);
        }
    }

    public function updateCard(Request $request)
    {
        $data = $request->all();
        $msg = [];
        $response = [];
        $valida = $this->validar($data,'update_card',['fluxo']);
        if(!empty($valida)){
            return response()->json(['erros'=>$valida],400);
        }
        try {
            $response['token'] = $this->getToken();
            $response['leads'] = [$data];
            $body = json_encode($response);
            $r = $this->client->request('POST', getenv('WS_AYTY_BASE') . "Card/Update", [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);
            $response = $r->getBody()->getContents();
            $respJson = json_decode($response);
            $msg = [];
            if(!isset($respJson->count_error)) {
                $msg['msg'] = "cartao_atualizado_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_gravar_cartao";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLog::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','atualiza card','',
                $response,$data['lead_id'],$data['lead_id']
            );
            file_put_contents(storage_path('logs/ayty_logs/update_card_'.date('Y-m-d').'-'.time().".txt"),$response);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch(\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/update_card_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            return response()->json(['message'=>'error','status'=>'error'],400);
        }
    }

    public function newPlan(Request $request)
    {
        $data = $request->all();
        $valida = $this->validar($data,'new_plan',null);
        if(!empty($valida)){
            return response()->json(['erros'=>$valida],400);
        }
        //passa o token para completar a requisição
        $response['token']=$this->getToken();
        try {

            $data['lead_status']='lead';
            $response['leads']=[$data];
            $body = json_encode($response);
            $r = $this->client->request('POST',getenv('WS_AYTY_BASE')."Plano/New",[
                'headers'=>['Content-Type'=> 'application/json'],
                'body'=>$body
            ]);
            $response = $r->getBody()->getContents();
            $respJson = json_decode($response);
            $msg = [];
            if(!isset($respJson->count_error)) {
                $msg['msg'] = "plano_atualizado_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_atualizar_lead";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLog::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','atualizar lead',
                AytyLog::formataLinhaPlanoAytyLog($data),$response,$data['lead_id'],''
            );
            file_put_contents(storage_path('logs/ayty_logs/new_plan_'.date('Y-m-d').'-'.time().".txt"),$response);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch (\Exception $e){
            file_put_contents(storage_path('logs/ayty_logs/new_plan_'.date('Y-m-d').'-'.time().".txt"),$e->getMessage());
            return response()->json(['message'=>'error','status'=>'error'],400);
        }

    }

    public function updatePlan(Request $request)
    {
        $data = $request->all();
        $msg = [];
        $response = [];
        $valida = $this->validar($data,'plan_update',null);
        if(!empty($valida)){
            return response()->json(['erros'=>$valida],400);
        }
        try {
            $response['token'] = $this->getToken();
            $response['leads'] = [$data];
            $body = json_encode($response);
            $r = $this->client->request('POST', getenv('WS_AYTY_BASE') . "Plano/Update", [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $body
            ]);
            $response = $r->getBody()->getContents();
            $respJson = json_decode($response);
            $msg = [];
            if($respJson->errors!="null") {
                $msg['msg'] = "lead_atualizado_sucesso";
                $msg['status'] = "success";
                $msg['code'] = 200;
            } else {
                $msg['msg']="erro_ao_gravar_lead";
                $msg['status'] = "error";
                $msg['code'] = 400;
            }

            AytyLog::saveAytyLog(
                $respJson->token,
                'ab->ayty','linha','update card',
                AytyLog::formataLinhaCardAytyLog($data),$response,$data['lead_id'],$data['lead_id']
            );
            //file_put_contents(storage_path('logs/ayty_logs/new_lead_'.date('Y-m-d').'-'.time().".txt"),$response);
            return response()->json(['message'=>$msg['msg'],'status'=>$msg['status']],$msg['code']);
        } catch(\Exception $e){
            return response()->json(['message'=>'error','status'=>'error'],400);
        }
    }

    public function simulaAytyInserts($data_inicial,$data_final)
    {
        if($data_final == "" || $data_inicial == "") return "Informe a data_inicial e data_final";
        set_time_limit(0);
        $importacao = 'importacao_leads_'.date('Y-m-d')."-".time().'.txt';
        echo "Iniciando teste de carga com dados falsos";
        echo "<pre>";
        //var_dump(Lead::where('come_id',0)); exit;
        $leads = Lead::select(['lead_nome','lead_id','lead_fone as lead_dddtelefone','lead_email'])
            ->where('come_id',0)
            ->where('lead_origem','<>','pdv')
            ->where('lead_criacao','>=',$data_inicial)
            ->where('lead_criacao','<=',$data_final)
            ->where('lead_status','Lead')->get()->toArray();
        //var_dump($leads[20]); exit;
        for($i=0;$i<count($leads);$i++):
            $msg=$leads[$i]['lead_nome']."-".$leads[$i]['lead_id'].'\n';
            $msg.= "------retorno-------";
            try {
                $r = $this->client->request('POST', 'http://agentebrasil.com/ws/wsabserver/new_lead', [
                    'form_params' => [
                        'lead_id'=>$leads[$i]['lead_id'],
                        'lead_nome'=>$leads[$i]['lead_nome'],
                        'lead_dddtelefone'=>$leads[$i]['lead_dddtelefone'],
                        'lead_email'=>$leads[$i]['lead_email'],
                        'lead_status'=>'lead',
                        'lead_ip'=>0,
                    ]
                ]);
                file_put_contents(storage_path('logs/'.$importacao),$msg.$r->getBody()->getContents().PHP_EOL,FILE_APPEND);
            } catch (\Exception $e){
                file_put_contents(storage_path('logs/'.$importacao),$msg.$e->getMessage().PHP_EOL,FILE_APPEND);
            }
        endfor;
    }

    public function simulaAytyUpdates()
    {
        echo "Iniciando teste de carga com dados falsos\n";
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Faker\Provider\pt_BR\PhoneNumber($faker));

        $arrLeads = [
            1481956739,1485699490,1478666996,
            1486167039,1484738312,1486788122,
            1479327488,1477872807,1484600136,
            1480211476];
        for($i=0;$i<count($arrLeads);$i++):
            $r = $this->client->request('POST','http://agentebrasil.com/ws/wsabserver/update_lead',[
                'form_params'=> [
                    'fluxo'=>rand(1,5),
                    'lead_id'=>$arrLeads[$i],
                    'lead_status'=>'ativo'
                ]
            ]);
            var_dump(json_decode($r->getBody()->getContents())); echo "\n";
        endfor;
    }
}
