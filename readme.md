# WebService Agente Brasil
***Descricao:*** Insere um novo comercial na base Ayty<br>
***Chamada:***[POST] /new_comecial<br />
***Parametros passados:***
```php
   plan_nome,
   plan_logica,
   plan_superlogica,
   plan_descricao,
   plan_parcelas,
   plan_valor
```
***Descricao:*** Insere um novo plano na base Ayty<br>
***Chamada:***[POST] /new_plan<br>
***Parametros passados:***
```php
   plan_nome,
   plan_logica,
   plan_superlogica,
   plan_descricao,
   plan_parcelas,
   plan_valor
```
***Descrição:*** Atualiza um plano na base Ayty <br>
***Chamada:***[POST] /plan_update<br>
***Parametros passados:***
```php
   plan_id,
   plan_nome,
   plan_logica,
   plan_superlogica,
   plan_descricao,
   plan_parcelas,
   plan_valor
```


***Descricao:*** Insere Leads vindos do facebook na base Ayty (À definir)<br>
***Chamada:***[POST] /new_lead_facebook(À definir)<br>


***Parametros passados:*** (À definir)


***Descricao:*** Insere Leads na base Ayty<br>
***Chamada:***[POST] /new_lead<br>
***Parametros passados:***
```php
   lead_id,
   lead_nome,
   lead_dddtelefone,
   lead_email,
   lead_ip,
   fluxo (opcional)
```

***Descricao:*** Atualiza Leads na base Ayty<br>
***Chamada:***[POST] /update_lead<br>
***Parametros passados:***
```php
   fluxo,
   lead_id,
   lead_status,
   fluxo
```


***Descricao:*** Atualiza um cartão unimed na base Ayty<br>
***Chamada:***[POST] /update_card<br>
***Parametros passados:***
```php
   lead_id,
   card_id,
   fluxo
```
